<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index',['categories'=> $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'gambar' => '',
            'judul' => '',
            'deskripsi' => '',
        ]);
        $array = $request->only([
            'gambar', 'judul', 'deskripsi',
        ]);
        $category = Category::create($array);
        return redirect()->route('categories.index')
            ->with('success_message', 'Berhasil menambah Konten baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        if (!$category) return redirect()->route('categories.index')
            ->with('error_message', 'Category dengan id '.$id.' tidak ditemukan');
        return view('categories.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|string|max:255',
            'deskripsi' => 'required|string|max:255'.$id,
        ]);
        $category = Category::find($id);
        $category->judul = $request->judul;
        $category->deskripsi = $request->deskripsi;
        return redirect()->route('categories.index')
            ->with('success_message', 'Berhasil Mengubah Konten');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if ($category) $category->delete();
        return redirect()->route('categories.index')
            ->with('success_message', 'Berhasil Menghapus Konten');
    }
} 