@extends('adminlte::page')

@section('title', 'Edit Konten')

@section('content_header')
    <h1 class="m-0 text-dark">Edit Konten</h1>
@stop

@section('content')
    <form action="{{route('categories.update', $category)}}" method="post">
        @method('PUT')
        @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="exampleInputGambar">Gambar</label>
                        <input type="file" class="form-control @error('deskripsi') is-invalid @enderror" placeholder="Masukkan Gambar" name="gambar" value="{{old('gambar')}}">
                        @error('gambar') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputJudul">Judul</label>
                        <input type="text" class="form-control @error('judul') is-invalid @enderror" id="exampleInputJudul" placeholder="Masukkan Judul" name="judul" value="{{$category->judul ?? old('judul')}}">
                        @error('judul') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputDeskripsi">Deskripsi</label>
                        <input type="text" class="form-control @error('deskripsi') is-invalid @enderror" id="exampleInputDeskripsi" placeholder="Masukkan Deskripsi" name="deskripsi" value="{{$category->deskripsi ?? old('deskripsi')}}">
                        @error('deskripsi') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('categories.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
@stop